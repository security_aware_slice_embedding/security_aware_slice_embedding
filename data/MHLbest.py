# -*- coding: utf-8 -*-
"""
Created on Thu Nov 14 16:34:11 2019

@author: jmila_h
"""
import pickle as pl
import matplotlib.colors as cl
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.tri as mtri
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import interactive
interactive(True)
from matplotlib import cm

figAR = plt.figure('Aceptance Rate (MH,Virtual link,SecBestEmbed)') # A, acceptance rate
figRevenueCost = plt.figure('RevenueCost (MH,Virtual link,SecBestEmbed)') # RC, revenueCost
figExTime = plt.figure('Execution Time (MH,Virtual link,SecBestEmbed)') # T, execution time
zfig=plt.figure('Samples (MH,Virtual link,SecBestEmbed)') # N and S domain plot

ARf = figAR.add_subplot(111, projection='3d') # A, acceptance rate
RevenueCostf = figRevenueCost.add_subplot(111, projection='3d') # R, revenue
ExTimef = figExTime.add_subplot(111, projection='3d')  # T, execution time
zf=zfig.add_subplot(111, projection='3d') # N and S domain plot


x3 = []  # N
y3 = [] # S 
AR=[]  # Acceptance rate
RevenueCost=[] # RevenueCost
ExTime=[] # Execution time
z=[] # N, S samples


#the algorithm performance when ignoring the security constraints, in the first line
AR0= 76# AcceptanceRate
RevenueCost0=float(4613.9840/12242.7971)
ExTime0=1.7368
#1.8947


#read the values form text
with open('./data/MHLbest.txt') as fobj: 
    for line in fobj: 
        row = line.split() 
        x3.append(float(row[-9]))#N value 
        y3.append(float(row[-8])) # S value
        AR.append(float(row[-7])-AR0) # AcceptanceRate
        if float(row[-5])!=0 :
            RevenueCost.append(float(row[-6])/float(row[-5])-RevenueCost0) # RevenueCost
        else:
            RevenueCost.append(0)
        ExTime.append(float(row[-3])-ExTime0) # Execution time
        z.append(float(0)) # 

    
dy = np.ones(len(x3)) # size of x axis
dx = np.ones(len(y3)) # size of y axis

################################################# AR figure ################################################# 
ARf.scatter(x3, y3, AR, marker='.', s=100, alpha=0.5, norm=None, c=AR, vmin=-1, vmax=1, cmap='coolwarm') 

ARf.view_init(elev=27, azim=32)

ARf.set_xlabel(r'$\mathcal{N}$',fontsize=20,labelpad=15)
ARf.set_xticks(np.arange(min(x3), max(x3)+2, int(max(x3)/10))) 
ARf.tick_params(axis='x', labelsize=8,pad=5, labelrotation=0)

ARf.set_ylabel(r'$\mathcal{S}$',fontsize=20,labelpad=15)
ARf.set_yticks(np.arange(min(y3), max(y3)+2, int(max(y3)/10)))
ARf.tick_params(axis='y', labelsize=8,pad=5, labelrotation=0)

ARf.set_zlabel(r'$\mathbb{A}$', fontsize=20,labelpad=15)
ARf.zaxis.set_rotate_label(False) 
ARf.tick_params(axis='z', labelsize=8,pad=5, labelrotation=0)
ARf.set_zlim3d(-80, 20)

################################################## RevnueCostFigure #################################################
RevenueCostf.scatter(x3,y3,RevenueCost,marker='.', s=100, alpha=0.5, norm=None, c=RevenueCost, vmin=-1, vmax=1, cmap='coolwarm')

RevenueCostf.view_init(elev=27, azim=32)

RevenueCostf.set_xlabel(r'$\mathcal{N}$',fontsize=20,labelpad=15)
RevenueCostf.set_xticks(np.arange(min(x3), max(x3)+2, int(max(x3)/10))) 
RevenueCostf.tick_params(axis='x', labelsize=8,pad=5, labelrotation=0)

RevenueCostf.set_ylabel(r'$\mathcal{S}$',fontsize=20,labelpad=15)
RevenueCostf.set_yticks(np.arange(min(y3), max(y3)+2, int(max(y3)/10)))
RevenueCostf.tick_params(axis='y', labelsize=8,pad=5, labelrotation=0)

RevenueCostf.set_zlabel(r'$\mathbb{RC}$',fontsize=20,labelpad=15)
RevenueCostf.zaxis.set_rotate_label(False)
RevenueCostf.tick_params(axis='z', labelsize=8,pad=5, labelrotation=0)
RevenueCostf.set_zlim3d(-1.5, 4)

################################################## Execution time figure #################################################
ExTimef.scatter(x3,y3,ExTime,marker='.', s=300, alpha=0.5, norm=None, c=ExTime, vmin=-1, vmax=1, cmap='coolwarm')

ExTimef.view_init(elev=27, azim=32)

ExTimef.set_xlabel(r'$\mathcal{N}$',fontsize=20,labelpad=15)
ExTimef.set_xticks(np.arange(min(x3), max(x3)+2, int(max(x3)/10)))
ExTimef.tick_params(axis='x', labelsize=8,pad=5, labelrotation=0)

ExTimef.set_ylabel(r'$\mathcal{S}$',fontsize=20,labelpad=15)
ExTimef.set_yticks(np.arange(min(y3), max(y3)+2, int(max(y3)/10)))
ExTimef.tick_params(axis='y', labelsize=8,pad=5, labelrotation=0)

ExTimef.set_zlabel(r'$\mathbb{T}$',fontsize=20,labelpad=15)
ExTimef.zaxis.set_rotate_label(False)
ExTimef.tick_params(axis='z', labelsize=8,pad=5, labelrotation=0)
ExTimef.set_zlim3d(-4, 4.2)

################################################## N and S domain figure #################################################
zf.scatter(x3,y3,z, c=z, marker='.', s=100, cmap='viridis', alpha=0.5)

zf.view_init(azim=9,elev=87)  

zf.set_xlabel(r'$\mathcal{N}$',fontsize=20, labelpad=20, rotation='horizontal') 
zf.set_xticks(np.arange(min(x3), max(x3)+2, int(max(x3)/10)))
zf.tick_params(axis='x', labelsize=13,pad=5, labelrotation=0)

zf.set_ylabel(r'$\mathcal{S}$',fontsize=20, labelpad=40) 
zf.set_yticks(np.arange(min(y3), max(y3)+2, int(max(y3)/10)))
zf.tick_params(axis='y', labelsize=13,pad=5, labelrotation=90)

zf.set_zticklabels(['']) 
zf.set_zlim3d(0,1) 
